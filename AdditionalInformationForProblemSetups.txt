Scenario 1
For scenario 1, only a Neumann boundary condition with a displacement of 0.2 m in y-direction was used. The current viscoel2pproblem.hh contains this setup.

Influence of viscosity

Viscosity can be either specified in the viscoel2pspatialparams.hh file or calculated in the viscoel2pmodel.hh file from the stress drop specified in the viscoel2pspatialparams.hh file.
Thus, for studying the influence of the viscosity, the respective lines in these file need to be commented and uncommented respectively. For the master thesis, the following values were used (in Pa s): 3 x 10^9, 6 x 10^9, 3 x 10^10, 6 x 10^10, 3 x 10^11, 3 x 10^12, 3 x 10^13, 3 x 10^14, 3 x 10^15.

Scenario 1
For scenario 2, a smaller displacement of 0.13045 m in y-direction was used and for the source a saturation of 3 x 10-3 was implemented. The current viscoel2pproblem.hh already contains the geometric restrictions for this source, but the respective line specifying the injection has to be uncommented. Similarily, the displacement boundary condition has to be changed for this setup, too.
