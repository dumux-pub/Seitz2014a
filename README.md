Summary

===============================

Dumux-Pub Module containing the code for producing the results published in:
G. Seitz
"The influence of induced fractures on the stress field of a coupled flow and geomechanics scenario"
Master's Thesis, 2014



Installation
============
The easiest way to install this module is to create a new folder and to execute the file
[installSeitz2014a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Seitz2014a/raw/master/installSeitz2014a.sh)
in this folder.

```bash
mkdir -p Seitz2014a && cd Seitz2014a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Seitz2014a/raw/master/installSeitz2014a.sh
sh ./installSeitz2014a.sh
```

For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Applications
============
The applications used for this publication can be found in test/geomechanics/viscoel2p
In order to run the executable, type e.g.:
```bash
cd dumux-Seitz2014a/test/geomechanics/viscoel2p
make test_viscoel2p
./test_viscoel2p grids/test_viscoel2p.dgf tEnd_Initialization tEnd dt
```


Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installSeitz2014a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Seitz2014a/raw/master/installSeitz2014a.sh).

In addition the following external software package are necessary for
compiling the executables:

| software           | version | type          |
| ------------------ | ------- | ------------- |
| automake           | 1.13.4  | build tool    |
| SuperLU            | 4.3     | linear solver |

The module has been checked for the following compilers:

| compiler      | version |
| ------------- | ------- |
| clang/clang++ | 3.5     |
| gcc/g++       | 4.7     |


