add_subdirectory("iapws")

install(FILES
        brine.hh
        ch4.hh
        co2.hh
        co2tablereader.hh
        co2tables.inc
        component.hh
        h2.hh
        h2o.hh
        n2.hh
        nullcomponent.hh
        o2.hh
        simpleco2.hh
        simpleh2o.hh
        tabulatedcomponent.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/material/components)
