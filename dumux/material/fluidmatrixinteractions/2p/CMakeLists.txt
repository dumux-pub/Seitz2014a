
install(FILES
        brookscorey.hh
        brookscoreyparams.hh
        efftoabslaw.hh
        efftoabslawparams.hh
        linearmaterial.hh
        linearmaterialparams.hh
        regularizedbrookscorey.hh
        regularizedbrookscoreyparams.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/material/fluidmatrixinteractions/2p)
