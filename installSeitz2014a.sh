#!/bin/sh

### Create a folder for the dune and dumux modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

### Clone the necessary modules
git clone https://gitlab.dune-project.org/core/dune-common.git
git clone https://gitlab.dune-project.org/core/dune-geometry.git
git clone https://gitlab.dune-project.org/core/dune-grid.git
git clone https://gitlab.dune-project.org/core/dune-istl.git
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone https://gitlab.dune-project.org/pdelab/dune-pdelab.git
git clone https://gitlab.dune-project.org/pdelab/dune-typetree.git
git clone https://gitlab.dune-project.org/extensions/dune-alugrid.git
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git dumux
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Seitz2014a.git dumux-Seitz2014a

### Go to specific branches
cd dune-common && git checkout releases/2.3 && cd ..
cd dune-geometry && git checkout releases/2.3 && cd ..
cd dune-grid && git checkout releases/2.3 && cd ..
cd dune-istl && git checkout releases/2.3 && cd ..
cd dune-localfunctions && git checkout releases/2.3 && cd ..
cd dune-alugrid && git checkout releases/2.3 && cd ..
cd dune-pdelab && git checkout releases/2.0 && cd ..
cd dune-typetree && git checkout releases/2.3 && cd ..
cd dumux && git checkout releases/2.6 && cd ..

### Run dunecontrol
./dune-common/bin/dunecontrol --opts=dumuxoptim.opts all

